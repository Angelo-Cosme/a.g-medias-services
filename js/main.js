  $(document).ready(function () {
    var openedPopup;
    $(".img-area .socials").click(function () {
      openedPopup = $(this).next(".modal");
      openedPopup.show();
    });

    $(".img-area .close-popup").click(function () {
      openedPopup.hide();
    });
});

/*
slider-area services
*/

$('.slider-area').slick({
  centerMode: true,
  dots: true,
  infinite: true,
  slidesToShow: 3,
  autoplay: true,
  speed: 400,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
});